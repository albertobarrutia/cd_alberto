Flask==1.1.1
Flask-RESTPlus==0.13.0
# esto incuir por BUG de Flask-restPlus
Werkzeug==0.16.1
importlib-metadata==1.0.0
Flask-SQLAlchemy==2.4.1
psycopg2-binary==2.8.4
pytest==5.3.1
ptvsd==4.3.2
gunicorn==20.0.4
pytest-cov==2.8.1
flake8==3.7.9
black==19.10b0
isort==4.3.21
pytest-xdist==1.30.0
