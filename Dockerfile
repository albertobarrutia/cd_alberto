FROM python:3.8.0-alpine


RUN apk update && \
    apk add --virtual build-deps gcc python-dev musl-dev && \
    apk add postgresql-dev && \
    apk add netcat-openbsd && \
    apk add build-base



ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV FLASK_ENV production
ENV APP_SETTINGS project.config.ProductionConfig


RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./requirements.txt .

RUN pip install -r requirements.txt



# Aqui igual es mejor hacer add en la fase de deasrrollo
COPY . .

RUN adduser -D myuser
USER myuser

CMD gunicorn --bind 0.0.0.0:$PORT manage:app

